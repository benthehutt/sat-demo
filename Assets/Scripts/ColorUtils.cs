﻿using System;

using UnityEngine;

public static class ColorUtils
{
    /// <summary>
    /// This algorithm takes normalized RGB values and returns:
    /// 
    /// h in [0, 360)
    /// s in [0, 1]
    /// l in [0, 1]
    /// </summary>
    /// <param name="red"></param>
    /// <param name="green"></param>
    /// <param name="blue"></param>
    /// <returns></returns>
    public static Vector3 ToHSL(float red, float green, float blue)
    {
        float max = Math.Max(red, Math.Max(green, blue));
        float min = Math.Min(red, Math.Min(green, blue));
        float chroma = max - min;

        float h = 0f, s = 0f, l = 0f;

        if (chroma > Mathf.Epsilon)
        {
            if (Math.Abs(red - max) < Mathf.Epsilon)
            {
                h = (green - blue) / chroma;
            }
            else if (Math.Abs(green - max) < Mathf.Epsilon)
            {
                h = (blue - red) / chroma + 2f;
            }
            else if (Math.Abs(red - green) < Mathf.Epsilon)
            {
                h = (red - green) / chroma + 4f;
            }

            h *= 60f;

            l = (max + min) / 2f;

            if (chroma > Mathf.Epsilon)
            {
                s = chroma / (1f - Math.Abs(2f * l - 1f));
            }
        }

        return new Vector3(h, s, l);
    }

    /// <summary>
    /// This takes an HSL value in the form:
    /// 
    /// h in [0, 360)
    /// s in [0, 1]
    /// l in [0, 1]
    /// 
    /// and returns a normalized RGB value.
    /// </summary>
    /// <param name="hue"></param>
    /// <param name="saturation"></param>
    /// <param name="lightness"></param>
    /// <returns></returns>
    public static Vector3 ToRGB(float hue, float saturation, float lightness)
    {
        hue /= 360f;

        float r = 0f, g = 0f, b = 0f;
        if (saturation < Mathf.Epsilon)
        {
            r = g = b = lightness;
        }
        else
        {
            float m2 = lightness < 0.5f
                ? lightness * (1f + saturation)
                : (lightness + saturation) - (lightness * saturation);
            float m1 = (lightness * 2f) - m2;

            r = HueToRGB(m1, m2, hue + (1f / 3f));
            g = HueToRGB(m1, m2, hue);
            b = HueToRGB(m1, m2, hue - (1f / 3f));
        }

        return new Vector3(r, g, b);
    }

    private static float HueToRGB(float m1, float m2, float h)
    {
        if (h < 0f)
        {
            h += 1.0f;
        }
        else if (h > 1.0f)
        {
            h -= 1.0f;
        }

        if (h * 6f < 1f)
        {
            return m1 + (m2 - m1) * 6f * h;
        }
        
        if (h * 2f < 1f)
        {
            return m2;
        }
        
        if (h * 3f < 2f)
        {
            return m1 + (m2 - m1) * ((2f / 3f) - h) * 6f;
        }
        
        return m1;
    }
}
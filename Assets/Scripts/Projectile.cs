﻿using UnityEngine;

/// <summary>
/// Simple projectile physics with a lifetime.
/// </summary>
public class Projectile : MonoBehaviour
{
    /// <summary>
    /// Keeps track of whether or not this has been initialized.
    /// </summary>
    private bool _initialized = false;

    /// <summary>
    /// The direction fired in.
    /// </summary>
    private Vector3 _direction;

    /// <summary>
    /// The amount of power fired with.
    /// </summary>
    private float _power;

    /// <summary>
    /// The lifetime of the projectile.
    /// </summary>
    private float _lifetime;

    /// <summary>
    /// How much time has elapsed.
    /// </summary>
    private float _timeElapsed;

    /// <summary>
    /// The bounds of the object.
    /// </summary>
    [SerializeField]
    private Bounds _bounds;

    /// <summary>
    /// The Bounds of the object. This is for rough collision detection.
    /// </summary>
    public Bounds Bounds
    {
        get
        {
            return new Bounds(
                transform.position,
                _bounds.size);
        }
    }

    /// <summary>
    /// An ICollidable implementation that wraps this object for collision
    /// detection.
    /// </summary>
    public ICollidable Collider
    {
        get;
        private set;
    }

    /// <summary>
    /// Initializes the projectile with how to fire.
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="direction"></param>
    /// <param name="power"></param>
    /// <param name="lifetime"></param>
    public void Initialize(
        Vector3 origin,
        Vector3 direction,
        float power,
        float lifetime)
    {
        if (_initialized)
        {
            return;
        }
        _initialized = true;

        transform.position = origin;

        _direction = direction;
        _power = power;
        _lifetime = lifetime;

        Collider = new CollisionCube(gameObject);
    }

    /// <summary>
    /// Updates the model with a time delta.
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public bool UpdateModel(float dt)
    {
        if (!_initialized)
        {
            return true;
        }

        _timeElapsed += Time.deltaTime;

        transform.position += _direction * _power * Time.deltaTime;
        
        if (_timeElapsed > _lifetime)
        {
            Destroy(gameObject);

            return false;
        }

        return true;
    }

    /// <summary>
    /// Draws gizmos.
    /// </summary>
    private void OnDrawGizmos()
    {
        GUI.color = Color.red;
        Gizmos.DrawWireCube(
            Bounds.center,
            Bounds.size);
    }
}
﻿public static class StringExtensions
{
    /// <summary>
    /// Reverses the words in a string.
    /// </summary>
    /// <param name="this"></param>
    /// <returns></returns>
    public static string ReverseWordWise(this string @this)
    {
        int len = @this.Length;
        char[] buffer = new char[len];

        // start by reversing whole string
        for (int i = len - 1; i >= 0; --i)
        {
            buffer[len - i - 1] = @this[i];
        }

        // now reverse each word
        int start = -1;
        for (int i = 0; i < len; i++)
        {
            if (' ' == @buffer[i])
            {
                if (-1 == start)
                {
                    continue;
                }

                ReverseWord(start, i - 1, ref buffer);

                start = -1;
            }
            else if (len - 1 == i)
            {
                if (-1 != start)
                {
                    ReverseWord(start, i, ref buffer);
                }
            }
            else if (-1 == start)
            {
                start = i;
            }
        }

        return new string(buffer);
    }

    /// <summary>
    /// Reverses the characters on a buffer in a given range.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="buffer"></param>
    private static void ReverseWord(int start, int end, ref char[] buffer)
    {
        int max = start + (end - start) / 2;
        
        for (int i = start; i <= max; i++)
        {
            char temp = buffer[i];
            buffer[i] = buffer[end - (i - start)];
            buffer[end - (i - start)] = temp;
        }
    }
}
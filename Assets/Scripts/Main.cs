﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Entry point for the application.
/// 
/// * With 1000+ cubes, it's clear that the space must be partitioned in some
/// way to cut down on comparisons between projectile and cube.
/// 
/// * With randomly distributed cubes, a fancy deata structure like an octree
/// is overkill. Instead, I've opted for a simple spatial hash.
/// 
/// * Every update, each projectile picks out the nearby cubes from the spatial
/// hash. This is broad phase collision detection.
/// 
/// * With this refined list cubes, I'm using a more precise collision
/// detection mechanism that will work with any convex polyhedra.
/// 
/// Further improvements that could be done:
/// 
/// * Little effort has gone into making the SAT implementation fast. The
/// Gizmos calls, for instance, add quite a bit of baggage, even if disabled.
/// 
/// * You'll notice that with projectiles moving this quickly, sometimes they
/// tunnel through cubes without hitting them. This could be improved in
/// several ways:
///     
///     1. Sweeping - here you would create a polyhedra by sweeping from the
///     last position to the next and test collisions on that geometry.
/// 
///     2. Multisampling - taking multiple samples per frame.
/// </summary>
public class Main : MonoBehaviour
{
    /// <summary>
    /// Cube placement constants.
    /// </summary>
    private const int MAX_RADIUS = 2000;
    private const int MIN_RADIUS = 100;
    private const int MAX_SIZE = 200;
    private const int MIN_SIZE = 100;
    private const int NUM_CUBES = 1000;

    /// <summary>
    /// Projectile constants.
    /// </summary>
    private const float MAX_POWER = 2000f;
    private const float MIN_POWER = 500f;
    private const float MAX_LIFETIME = 4f;
    private const float MIN_LIFETIME = 3f;

    /// <summary>
    /// The spatial hash for storing cubes.
    /// </summary>
    private readonly SpatialHash _hash = new SpatialHash(
        new Bounds(
            new Vector3(),
            2f * new Vector3(
                MAX_RADIUS,
                MAX_RADIUS,
                MAX_RADIUS)),
        256f);

    /// <summary>
    /// A double buffer for storing projectiles.
    /// </summary>
    private readonly List<Projectile>[] _projectiles = new []
    {
        new List<Projectile>(),
        new List<Projectile>()
    };

    /// <summary>
    /// Index for swapping between buffers.
    /// </summary>
    private int _projectileBufferIndex = 0;

    /// <summary>
    /// A reused list for storing cubes retrieved by the broadphase collision
    /// detection.
    /// </summary>
    private readonly List<Cube> _possibleIntersections = new List<Cube>(); 
    
    /// <summary>
    /// Array of all cubes.
    /// </summary>
    private Cube[] _cubes;

    /// <summary>
    /// Controls how quickly the camera rotates.
    /// </summary>
    [SerializeField]
    private float _rotateSpeed = 1.5f;

    /// <summary>
    /// The prefab to instance for a cube.
    /// </summary>
    [SerializeField]
    private Cube _cubePrefab;

    /// <summary>
    /// The prefab to instance for a projectile.
    /// </summary>
    [SerializeField]
    private Projectile _projectilePrefab;

    /// <summary>
    /// Initializes the scene.
    /// </summary>
	private void Start ()
	{
	    var root = new GameObject("CubedRoot");

	    _cubes = new Cube[NUM_CUBES];
        for (int i = 0, len = _cubes.Length; i < len; i++)
        {
            var cube = _cubes[i] = (Cube) Instantiate(_cubePrefab);
            cube.Initialize(
                new Vector3(
                    RandSign() * Random.Range(MIN_RADIUS, MAX_RADIUS),
                    RandSign() * Random.Range(MIN_RADIUS, MAX_RADIUS),
                    RandSign() * Random.Range(MIN_RADIUS, MAX_RADIUS)), 
                Random.Range(MIN_SIZE, MAX_SIZE),
                Quaternion.Euler(Random.insideUnitCircle * 180f / Mathf.PI));
            cube.transform.parent = root.transform;

            _hash.Add(cube);
        }
	}
	
    /// <summary>
    /// Called on every update tick.
    /// </summary>
	private void FixedUpdate () {
	    UpdateInput();
        UpdateProjectilePositions();

	    DetectCollisions();
	}

    /// <summary>
    /// Polls for input.
    /// </summary>
    private void UpdateInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Camera.main.transform.Rotate(Vector3.up, -_rotateSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Camera.main.transform.Rotate(Vector3.up, _rotateSpeed);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Camera.main.transform.Rotate(Vector3.right, -_rotateSpeed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Camera.main.transform.Rotate(Vector3.right, _rotateSpeed);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Fire();
        }
    }

    /// <summary>
    /// Moves the projectiles based on how much time has passed.
    /// </summary>
    private void UpdateProjectilePositions()
    {
        List<Projectile> projectileBuffer = _projectiles[_projectileBufferIndex];
        List<Projectile> nextProjectileBuffer = _projectiles[(_projectileBufferIndex = (_projectileBufferIndex + 1) % 2)];

        float delta = Time.deltaTime;
        for (int i = 0, len = projectileBuffer.Count; i < len; i++)
        {
            Projectile projectile = projectileBuffer[i];
            if (projectile.UpdateModel(delta))
            {
                nextProjectileBuffer.Add(projectile);
            }
        }

        projectileBuffer.Clear();
    }

    /// <summary>
    /// Detects collisions between projectiles and cubes.
    /// </summary>
    private void DetectCollisions()
    {
        List<Projectile> projectiles = _projectiles[_projectileBufferIndex];
        for (int i = 0, len = projectiles.Count; i < len; i++)
        {
            // broad phase
            _possibleIntersections.Clear();

            Projectile projectile = projectiles[i];
            _hash.Gather(projectile.Bounds, _possibleIntersections);

            // narrow phase
            for (int j = 0, jlen = _possibleIntersections.Count; j < jlen; j++)
            {
                if (CollisionDetector.IsCollision(_possibleIntersections[j].Collider, projectiles[i].Collider))
                {
                    _possibleIntersections[j].Hit = true;
                }
            }
        }
    }

    /// <summary>
    /// Spawns a new projectile and fires it forward.
    /// </summary>
    private void Fire()
    {
        var projectile = (Projectile) Instantiate(_projectilePrefab);
        projectile.Initialize(
            Camera.main.transform.position,
            Camera.main.transform.forward,  
            Random.Range(MIN_POWER, MAX_POWER),
            Random.Range(MIN_LIFETIME, MAX_LIFETIME));
        _projectiles[_projectileBufferIndex].Add(projectile);
    }

    /// <summary>
    /// Randomly returns a -1 or 1.
    /// </summary>
    /// <returns></returns>
    private static int RandSign()
    {
        return Random.Range(0f, 1f) > 0.5f
            ? 1
            : -1;
    }
}
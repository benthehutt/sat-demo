﻿using System.Collections.Generic;

using UnityEngine;

/// <summary>
/// Simple interface for members of the hash.
/// </summary>
public interface ISpatial
{
    Vector3 Position { get; }

    int BinX { get; set; }
    int BinY { get; set; }
    int BinZ { get; set; }
}

/// <summary>
/// A simple hash for items in 3D space.
/// </summary>
public class SpatialHash
{
    /// <summary>
    /// The capacity of the created lists.
    /// </summary>
    private const int DEFAULT_BIN_CAPACITY = 4;

    /// <summary>
    /// The bounds of the hash.
    /// </summary>
    private readonly Bounds _bounds;

    /// <summary>
    /// A speed up... Essentially stores how to divide positions.
    /// </summary>
    private readonly int _cellShift = 0;

    /// <summary>
    /// A list of all elements in the hash.
    /// </summary>
    private readonly List<ISpatial> _allElements = new List<ISpatial>();

    /// <summary>
    /// The array of hash bins.
    /// </summary>
    private readonly List<ISpatial>[,,] _hash;

    /// <summary>
    /// The width of the hash.
    /// </summary>
    private readonly int _hashWidth;    // x

    /// <summary>
    /// The height of the hash.
    /// </summary>
    private readonly int _hashHeight;   // y

    /// <summary>
    /// The depth of the hash.
    /// </summary>
    private readonly int _hashDepth;    // z

    /// <summary>
    /// All elements of the hash.
    /// </summary>
    public List<ISpatial> Elements
    {
        get
        {
            return _allElements;
        }
    } 

    /// <summary>
    /// Creates a new hash with a minimum cell size.
    /// </summary>
    /// <param name="bounds"></param>
    /// <param name="minCellSize"></param>
    public SpatialHash(Bounds bounds, float minCellSize)
    {
        _bounds = bounds;

        while (Mathf.Pow(2f, ++_cellShift) < minCellSize) { }

        _hashWidth = ((int) _bounds.size.x >> _cellShift) + 1;
        _hashHeight = ((int) _bounds.size.y >> _cellShift) + 1;
        _hashDepth = ((int) _bounds.size.z >> _cellShift) + 1;

        int cellWidth = 1 << _cellShift;

        _bounds.size = new Vector3(
            _hashWidth * cellWidth,
            _hashHeight * cellWidth,
            _hashDepth * cellWidth);

        _hash = new List<ISpatial>[_hashWidth, _hashHeight, _hashDepth];
        for (int x = 0, xlen = _hash.GetLength(0); x < xlen; x++)
        {
            for (int y = 0, ylen = _hash.GetLength(1); y < ylen; y++)
            {
                for (int z = 0, zlen = _hash.GetLength(2); z < zlen; z++)
                {
                    _hash[x, y, z] = new List<ISpatial>(DEFAULT_BIN_CAPACITY);
                }
            }
        }
    }

    /// <summary>
    /// Adds an element to the hash.
    /// </summary>
    /// <param name="element"></param>
    public void Add(ISpatial element)
    {
        int x, y, z;
        GetBinIndices(element, out x, out y, out z);

        element.BinX = x;
        element.BinY = y;
        element.BinZ = z;

        _hash[x, y, z].Add(element);
        _allElements.Add(element);
    }

    /// <summary>
    /// Returns all elements within the bounds.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="bounds"></param>
    /// <param name="results"></param>
    public void Gather<T>(Bounds bounds, List<T> results) where T : ISpatial
    {
        int minx, maxx, miny, maxy, minz, maxz;
        ToHashSpace(
            bounds,

            out minx,
            out maxx,
            
            out miny,
            out maxy,
            
            out minz,
            out maxz);

        for (int z = minz; z < maxz; z++)
        {
            for (int y = miny; y < maxy; y++)
            {
                for (int x = minx; x < maxx; x++)
                {
                    List<ISpatial> elements = _hash[x, y, z];
                    for (int i = 0, len = elements.Count; i < len; i++)
                    {
                        results.Add((T) elements[i]);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Transforms a Bounds object from world to hash space.
    /// </summary>
    /// <param name="bounds"></param>
    /// <param name="minx"></param>
    /// <param name="maxx"></param>
    /// <param name="miny"></param>
    /// <param name="maxy"></param>
    /// <param name="minz"></param>
    /// <param name="maxz"></param>
    private void ToHashSpace(
        Bounds bounds,

        out int minx,
        out int maxx,
        
        out int miny,
        out int maxy,
        
        out int minz,
        out int maxz)
    {
        minx = Mathf.Max(0, Mathf.Min(_hashWidth - 1, (int) (-_bounds.min.x + bounds.min.x) >> _cellShift));
        maxx = Mathf.Max(0, Mathf.Min(_hashWidth - 1, (int) (-_bounds.min.x + bounds.max.x) >> _cellShift) + 1);

        miny = Mathf.Max(0, Mathf.Min(_hashHeight - 1, (int) (-_bounds.min.y + bounds.min.y) >> _cellShift));
        maxy = Mathf.Max(0, Mathf.Min(_hashHeight - 1, (int) (-_bounds.min.y + bounds.max.y) >> _cellShift) + 1);

        minz = Mathf.Max(0, Mathf.Min(_hashDepth - 1, (int) (-_bounds.min.z + bounds.min.z) >> _cellShift));
        maxz = Mathf.Max(0, Mathf.Min(_hashDepth - 1, (int) (-_bounds.min.z + bounds.max.z) >> _cellShift) + 1);
    }

    /// <summary>
    /// Retrieves the indices into the hash.
    /// </summary>
    /// <param name="element"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    private void GetBinIndices(
        ISpatial element,
        out int x,
        out int y,
        out int z)
    {
        x = Mathf.Max(0, Mathf.Min(_hashWidth - 1, (int) (-_bounds.min.x + element.Position.x) >> _cellShift));
        y = Mathf.Max(0, Mathf.Min(_hashHeight - 1, (int) (-_bounds.min.y + element.Position.y) >> _cellShift));
        z = Mathf.Max(0, Mathf.Min(_hashDepth - 1, (int) (-_bounds.min.z + element.Position.z) >> _cellShift));
    }
}